package com.rebtsoft.research.bestival.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rebtsoft.research.bestival.Objects.TrackList;
import com.rebtsoft.research.bestival.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * TrackListAdapter
 */
public class TrackListAdapter extends BaseAdapter {
    ArrayList<TrackList> tracks;
    Context con;

    public TrackListAdapter(Context con, ArrayList<TrackList> tracks) {
        this.tracks = tracks;
        this.con = con;
    }

    @Override
    public int getCount() {
        return tracks.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.track_row, parent, false);

        TextView name = (TextView) convertView.findViewById(R.id.name_holder);
        TextView title = (TextView) convertView.findViewById(R.id.title_holder);
        ImageView btn = (ImageView) convertView.findViewById(R.id.play);


        name.setText(tracks.get(position).getBandname());
        title.setText(tracks.get(position).getSong());
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                con.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("deezer://www.deezer.com/search/" + tracks.get(position).getBandname())));
            }
        });

        return convertView;
    }


}
