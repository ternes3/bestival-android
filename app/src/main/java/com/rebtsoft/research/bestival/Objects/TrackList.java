package com.rebtsoft.research.bestival.Objects;


/**
 * TrackList
 * @since eben
 */
public class TrackList {
    private String bandname;
    private String song;
    private Integer albumid;

    public TrackList(String bandname, String song, Integer albumid) {
        this.bandname = bandname;
        this.song = song;
        this.albumid = albumid;
    }

    public String getBandname() {
        return bandname;
    }

    public void setBandname(String bandname) {
        this.bandname = bandname;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public Integer getAlbumid() {
        return albumid;
    }

    public void setAlbumid(Integer albumid) {
        this.albumid = albumid;
    }
}
