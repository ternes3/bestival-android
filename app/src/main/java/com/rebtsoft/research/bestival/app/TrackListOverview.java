package com.rebtsoft.research.bestival.app;

import android.app.Activity;

import android.os.Bundle;
import android.os.StrictMode;

import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rebtsoft.research.bestival.Objects.TrackList;
import com.rebtsoft.research.bestival.R;
import com.rebtsoft.research.bestival.adapters.TrackListAdapter;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



/**
 * TrackListOverview
 * @since eben
 */
@EActivity
public class TrackListOverview extends Activity {
    ArrayList<TrackList> tracks;
    String res = null;
    String festival;
    String date;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_event_overview);
        tracks = new ArrayList<>();

        festival = getIntent().getStringExtra("festival");
        date = getIntent().getStringExtra("date");

        //do stuff in background
        doinbackground();
    }


    @Background
    public void doinbackground() {
        makeRequest();
    }

    @UiThread
    public void updateUI() {
        ProgressBar bar = (ProgressBar) findViewById(R.id.progress);
        bar.setVisibility(View.INVISIBLE);
        ListView list = (ListView) findViewById(R.id.list);
        list.setAdapter(new TrackListAdapter(this, tracks));
    }

    private String makeRequest() {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://178.254.31.43:9014/lineups";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        res = response;
                        try {
                            JSONObject test = new JSONObject(response);
                            JSONArray array = new JSONArray(test.get("acts").toString());

                            for(int i = 0; array.length() > i;i++) {
                                tracks.add(new TrackList(array.get(i).toString(), "Band", 12231));
                            }
                            updateUI();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("name", festival);
                params.put("year", date);
                return params;
            }
        };
        queue.add(postRequest);

        return res;
    }
}