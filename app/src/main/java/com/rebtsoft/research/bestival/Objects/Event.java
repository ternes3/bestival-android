package com.rebtsoft.research.bestival.Objects;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ternes3 on 12.09.15.
 */
public class Event {
    private String name;
    private Date startName;

    public Event(String name, Date startName) {
        this.name = name;
        this.startName = startName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartName() {
        return startName;
    }

    public void setStartName(Date startName) {
        this.startName = startName;
    }
}
