package com.rebtsoft.research.bestival.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.deezer.sdk.player.AlbumPlayer;

import com.rebtsoft.research.bestival.Objects.ActObject;
import com.rebtsoft.research.bestival.Objects.Event;
import com.rebtsoft.research.bestival.R;
import com.rebtsoft.research.bestival.adapters.ActListAdapter;
import com.rebtsoft.research.bestival.ui.Utility;

import java.util.ArrayList;

public class EventOverviewActivity extends Activity
{
    ArrayList<Event> events;
    AlbumPlayer albumPlayer;
    ArrayList<ActObject> acts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_overview);

        Utility util = new Utility();
        events = util.readCalendarEvent(this);
        for(int i = 0; events.size() > i;i++) {
            Log.d("DOG", events.get(i).getName());

            if(events.get(i).getName().toLowerCase().contains("Festival".toLowerCase())){
                 acts.add(new ActObject(events.get(i).getName().replaceAll("[Ff]estival", "") + util.getDate(events.get(i).getStartName()), R.drawable.finaltent));
            } else if(events.get(i).getName().toLowerCase().contains("Konzert".toLowerCase())) {
                 acts.add(new ActObject(events.get(i).getName().replaceAll("[Kk]onzert", "") + util.getDate(events.get(i).getStartName()), R.drawable.finalconcert));
            }
        }

        ProgressBar bar = (ProgressBar) findViewById(R.id.progress);
        bar.setVisibility(View.INVISIBLE);

        ListView list = (ListView) findViewById(R.id.list);
        list.setAdapter(new ActListAdapter(this, acts));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Utility util = new Utility();
                Intent intents = new Intent();
                intents.setClass(EventOverviewActivity.this, TrackListOverview_.class);
                intents.putExtra("festival", acts.get(position).getName().substring(0, acts.get(position).getName().length() - 5));
                intents.putExtra("date", acts.get(position).getName().substring( acts.get(position).getName().length() - 4));
                intents.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intents);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_event_overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
