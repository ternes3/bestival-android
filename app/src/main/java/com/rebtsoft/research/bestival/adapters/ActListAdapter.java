package com.rebtsoft.research.bestival.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rebtsoft.research.bestival.Objects.ActObject;
import com.rebtsoft.research.bestival.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * ActListAdapter
 */
public class ActListAdapter extends BaseAdapter {
    ArrayList<ActObject> act;
    Context con;

    public ActListAdapter(Context con, ArrayList<ActObject> act) {
        this.con = con;
        this.act = act;
    }

    @Override
    public int getCount() {
        return act.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        convertView = inflater.inflate(R.layout.act_row, parent, false);

        ImageView image = (ImageView) convertView.findViewById(R.id.image_holder);
        TextView text = (TextView) convertView.findViewById(R.id.name_holder);

        text.setText(act.get(position).getName());

        Picasso.with(con)
                .load(act.get(position).getImage())
                .placeholder(R.drawable.finalcrowd)
                .error(R.drawable.finaltent)
                .into(image);

        return convertView;
    }
}
