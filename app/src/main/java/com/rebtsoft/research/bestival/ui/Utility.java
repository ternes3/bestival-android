package com.rebtsoft.research.bestival.ui;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.rebtsoft.research.bestival.Objects.Event;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Utility {
    public static String nameOfEvent;
    public static Date startDates;

    public static ArrayList<Event> events = new ArrayList<>();

    public static ArrayList<Event> readCalendarEvent(Context context) {
        Cursor cursor = parseCalendarEntries(context);
        String CNames[] = fetchCalenderNames(cursor);

        clearArraylist();
        for (int i = 0; i < CNames.length; i++) {
            extractItemsfromCalendarEvent(cursor, CNames, i);
            events.add(new Event(nameOfEvent, startDates));
        }

        return events;
    }

    private static void clearArraylist() {
        events.clear();
    }

    private static Cursor parseCalendarEntries(Context context) {
        Cursor cursor = context.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"}, null,
                        null, null);
        cursor.moveToFirst();
        return cursor;
    }

    private static void extractItemsfromCalendarEvent(Cursor cursor, String[] CNames, int i) {
        nameOfEvent = cursor.getString(1);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(Long.valueOf(cursor.getString(3)));
        startDates =  cal.getTime();
        CNames[i] = cursor.getString(1);
        cursor.moveToNext();
    }

    private static String[] fetchCalenderNames(Cursor cursor) {
        return new String[cursor.getCount()];
    }

    public static String getDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return formatter.format(calendar.getTime());
    }

}